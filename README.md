# README #
CLI application - Email crawler using GATE and JAPE grammar

Input: text (e.g. webpage)

Output: XML containing found email addresses

# RUN #
java -jar "GATE-embedded-hw.jar" "<tr><td>Ing. Matěj</td><td>katedra číslicového návrhu</td><td>KČN<br/>KČN</td><td>matej@fit.abc.cz</td><td>+420-12345-1234</td><td>A-1054</td></tr><tr><td>Ing. Václav, Ph.D.</td><td>katedra softwarového inženýrství</td><td>KSI</td><td>vaclav.abcd@fit.abc.cz</td><td>&nbsp;</td><td>&nbsp;</td></tr><tr><td>PhDr. Mgr. Zdeňka</td><td>katedra softwarového inženýrství</td><td>KSI</td><td>zdenka.xyz@it.abc.cz</td><td>&nbsp;</td><td>&nbsp;</td></tr>"

# OUTPUT #
<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<emails>
  <email address="matej@fit.abc.cz"/>
  <email address="vaclav.abcd@fit.abc.cz"/>
  <email address="zdenka.xyz@it.abc.cz"/>
</emails>