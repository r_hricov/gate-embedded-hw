
package cz.ctu.fit.ddw;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;

public class GATEEmbedded {

    public static void main(String[] args) throws TransformerConfigurationException, TransformerException {
        org.apache.log4j.Logger.getRootLogger().setLevel(org.apache.log4j.Level.OFF);
        try {
            GateClient client = new GateClient();        
            client.run(args[0]);    
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(GATEEmbedded.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
