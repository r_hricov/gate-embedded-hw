
package cz.ctu.fit.ddw;

import gate.Annotation;
import gate.AnnotationSet;
import gate.Corpus;
import gate.CreoleRegister;
import gate.Document;
import gate.Factory;
import gate.FeatureMap;
import gate.Gate;
import gate.Node;
import gate.ProcessingResource;
import gate.creole.SerialAnalyserController;
import gate.util.GateException;
import java.io.File;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Attr;
import org.w3c.dom.Element;

public class GateClient {
    
    // corpus pipeline
    private static SerialAnalyserController annotationPipeline = null;
    
    // whether the GATE is initialised
    private static boolean isGateInitilised = false;
    private DocumentBuilderFactory docFactory = null;
    private DocumentBuilder docBuilder = null;
    private org.w3c.dom.Document xml = null;
    private Element root = null;
    
    public void run(String text) throws TransformerConfigurationException, TransformerException, ParserConfigurationException{
        
        if(!isGateInitilised){
            
            // initialise GATE
            initialiseGate();            
        }    
        
        initXML();

        try {                
            // create an instance of a Document Reset processing resource
            ProcessingResource documentResetPR = (ProcessingResource) Factory.createResource("gate.creole.annotdelete.AnnotationDeletePR");

            // create an instance of a English Tokeniser processing resource
            ProcessingResource tokenizerPR = (ProcessingResource) Factory.createResource("gate.creole.tokeniser.DefaultTokeniser");
            
            ProcessingResource testAnnieGazeter = (ProcessingResource) Factory.createResource("gate.creole.gazetteer.DefaultGazetteer");
           
            // locate the JAPE grammar file
            File japeOrigFile = new File("C:/Users/Radko/Documents/NetBeansProjects/GATE-embedded-hw/src/cz/ctu/fit/ddw/hwGrammar.JAPE");
            java.net.URI japeURI = japeOrigFile.toURI();
            
            // create feature map for the transducer
            FeatureMap transducerFeatureMap = Factory.newFeatureMap();
            try {
                // set the grammar location
                transducerFeatureMap.put("grammarURL", japeURI.toURL());
                // set the grammar encoding
                transducerFeatureMap.put("encoding", "UTF-8");
            } catch (MalformedURLException e) {
                System.out.println("Malformed URL of JAPE grammar");
                System.out.println(e.toString());
            }
            
            // create an instance of a JAPE Transducer processing resource
            ProcessingResource japeTransducerPR = (ProcessingResource) Factory.createResource("gate.creole.Transducer", transducerFeatureMap);

            ProcessingResource annieGazeter = (ProcessingResource) Factory.createResource("gate.creole.gazetteer.DefaultGazetteer");
           
            // create corpus pipeline
            annotationPipeline = (SerialAnalyserController) Factory.createResource("gate.creole.SerialAnalyserController");

            // add the processing resources (modules) to the pipeline
            annotationPipeline.add(documentResetPR);
            annotationPipeline.add(tokenizerPR);
            annotationPipeline.add(annieGazeter);
            annotationPipeline.add(japeTransducerPR);
            
            Document document1 = Factory.newDocument(text);

            Corpus corpus = Factory.newCorpus("");
            corpus.add(document1);

            // set the corpus to the pipeline
            annotationPipeline.setCorpus(corpus);

            //run the pipeline
            annotationPipeline.execute();

            // loop through the documents in the corpus
            for(int i=0; i< corpus.size(); i++){

                Document doc = corpus.get(i);

                // get the default annotation set
                AnnotationSet as_default = doc.getAnnotations();

                FeatureMap futureMap = null;
                // get all Token annotations
                AnnotationSet annSetEmail = as_default.get("Email",futureMap);
                //System.out.println("Number of addresses: " + annSetEmail.size());
                
                ArrayList tokenEmail = new ArrayList(annSetEmail);

                for (int j = 0; j < tokenEmail.size(); j++) {
                    Annotation token = (Annotation)tokenEmail.get(j);
                    Node isaStart = token.getStartNode();
                    Node isaEnd = token.getEndNode();    
                    String underlyingString = doc.getContent().getContent(isaStart.getOffset(), isaEnd.getOffset()).toString();

                    Element foo = xml.createElement("email");
                    Attr bar = xml.createAttribute("address");
                    //set email as attribute value
                    bar.setValue(underlyingString);
                    foo.setAttributeNode(bar);
                    root.appendChild(foo);
                }
                TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
                StringWriter writer = new StringWriter();
                transformer.transform(new DOMSource(xml), new StreamResult(writer));
                System.out.println(writer.getBuffer().toString());
            }
        } catch (GateException ex) {
            Logger.getLogger(GateClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void initXML() throws ParserConfigurationException{
        docFactory = DocumentBuilderFactory.newInstance();
        docBuilder = docFactory.newDocumentBuilder();
        xml = (org.w3c.dom.Document) docBuilder.newDocument();
        root = xml.createElement("emails");
        xml.appendChild(root);
    }

    private void initialiseGate() {
 
        try {
            // set GATE home folder
            // Eg. /Applications/GATE_Developer_7.0
            File gateHomeFile = new File("C:/Program Files/GATE_Developer_8.1");
            Gate.setGateHome(gateHomeFile);
            
            // set GATE plugins folder
            // Eg. /Applications/GATE_Developer_7.0/plugins            
            File pluginsHome = new File("C:/Program Files/GATE_Developer_8.1/plugins");
            Gate.setPluginsHome(pluginsHome);                     
            
            // initialise the GATE library
            Gate.init();
            
            // load ANNIE plugin
            CreoleRegister register = Gate.getCreoleRegister();
            URL annieHome = new File(pluginsHome, "ANNIE").toURL();
            register.registerDirectories(annieHome);
            
            // flag that GATE was successfuly initialised
            isGateInitilised = true;
            
        } catch (MalformedURLException ex) {
            Logger.getLogger(GateClient.class.getName()).log(Level.SEVERE, null, ex);
        } catch (GateException ex) {
            Logger.getLogger(GateClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    
}